import React from 'react';
import styles from './Card.module.css';

export const Card = (props) => {
    const { cardData, flipped } = props;
    const cardClassName = `${styles.card} ${flipped ? styles.flipped : styles.unflipped}`;
    return (
        <div className={cardClassName} >
            {flipped && <p className={styles.text}>{cardData}</p>}
        </div>
    );
};