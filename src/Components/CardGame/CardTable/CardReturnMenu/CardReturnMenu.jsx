import React, { useState } from 'react';
import styles from './CardReturnMenu.module.css';

export const CardReturnMenu = (props) => {
    const { cards, returnCards } = props;
    const [selectedCards, setSelectedCards] = useState([]);
    return (
        <div className={styles.cardReturnMenu}>
            <div className="description">
                Select cards to return to the deck
            </div>
            <div className={styles.cardsAvailableToReturn}>
                {cards.map((card) => {
                    const selected = selectedCards.includes(card);
                    const menuCardClass = `${styles.menuCard} ${selected ? styles.selected : ""}`;
                    return (
                        <div
                            className={menuCardClass}
                            onClick={() => {
                                if (selected) {
                                    setSelectedCards(selectedCards.filter((selectedCard) => selectedCard !== card));
                                }
                                else {
                                    setSelectedCards(selectedCards.concat(card));
                                }
                            }}
                        >
                            {card}
                        </div>
                    );
                })}
            </div>
            <div className={styles.returnButton}>
                <button onClick={() => {
                    returnCards(selectedCards);
                    setSelectedCards([]);
                }}>
                    Return selected cards
                </button>
            </div>
        </div>
    );
}