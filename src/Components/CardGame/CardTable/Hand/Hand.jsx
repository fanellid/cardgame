import React from 'react';
import { Card } from "../Card/Card";
import styles from './Hand.module.css';

export const Hand = (props) => {
    const { userName, cards } = props;

    return (
        <div className={styles.hand}>
            <div className={styles.cards}>
                {cards.map((card) => <Card cardData={card} flipped={true} />)}
            </div>
            <div className={styles.userName}>
                {userName}
            </div>
        </div>
    );
};