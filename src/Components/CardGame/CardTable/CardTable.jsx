import React, { useState } from 'react';
import { useCardGameState } from '../../../hooks/useCardGameState';
import { CardReturnMenu } from './CardReturnMenu/CardReturnMenu';
import { Deck } from './Deck/Deck';
import { Hand } from './Hand/Hand';
import styles from './CardTable.module.css';

const maxCardsPerPlayer = 3;

export const CardTable = (props) => {
    const { players, cards } = props;
    const { cardGameState, dealCardToPlayer, returnCardsToDeck } = useCardGameState(players, cards, maxCardsPerPlayer);
    const { deck, playerHands, shuffling, ...rest } = cardGameState;
    const [activeUser, setActiveUser] = useState(players[0].userName);

    return (
        <div className={`${styles.cardTable} ${shuffling ? styles.shuffling : ""}`}>
            <PlayerSelectMenu players={players} handleChange={(e) => { setActiveUser(e.currentTarget.value); }} />
            {shuffling && (<div>shuffling, hang on!</div>)}
            <Deck
                cards={deck}
                dealCard={(card) => {
                    if (playerHands[activeUser].length < maxCardsPerPlayer) {
                        dealCardToPlayer(activeUser, card);
                    }
                }}
            />
            <div className={styles.hands}>
                {
                    players.map((player) => {
                        const hand = playerHands[player.userName];
                        return (<Hand userName={player.userName} cards={hand} />);
                    })
                }
            </div>
            <CardReturnMenu
                cards={playerHands[activeUser]}
                returnCards={(cardsToReturn) => { returnCardsToDeck(activeUser, cardsToReturn); }}
            />
        </div>
    );
};

const PlayerSelectMenu = (props) => {
    const { players, handleChange } = props;
    return (
        <div className="playerSelect">
            <span>select user </span>
            <select onChange={handleChange}>
                {players.map((player) => <option value={player.userName}>{player.userName}</option>)}
            </select>
        </div>
    );
};