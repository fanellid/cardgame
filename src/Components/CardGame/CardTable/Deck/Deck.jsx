import React from 'react';
import { Card } from '../Card/Card';
import styles from './Deck.module.css';

export const Deck = (props) => {
    const { cards, dealCard } = props;
    return (
        <div className={styles.deck}>
        {cards.map((card) =>
            <div className={styles.deckCard} onClick={() => { dealCard(card); }}>
                <Card cardData={card} flipped={false} />
            </div>
        )}
        </div>
    );
};