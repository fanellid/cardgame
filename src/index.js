import * as React from "react";
import * as ReactDOM from "react-dom";
import { CardTable } from "./Components/CardGame/CardTable/CardTable";

const App = () => (
  <div className="main">
    <CardTable players={[{userName:'batman'},{userName:'fflintstone@qanon.net'},{userName:'chewbacca'}]} cards={['TSLA','NVDA','META','MCDS','THERANOS','ENRON','QANON','TIKTOK','PHUB']} />
  </div>
);

ReactDOM.render(<App />, document.querySelector("#root"));

