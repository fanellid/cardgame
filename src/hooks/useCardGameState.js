import React, { useEffect, useReducer } from "react";

const actions = {
    CARD_DEALT_TO_PLAYER: "CARD_DEALT_TO_PLAYER",
    CARDS_RETURNED_TO_DECK: "CARDS_RETURNED_TO_DECK",
    GAME_ENDED: "GAME_ENDED",
    GAME_STARTED: "GAME_STARTED",
    SHUFFLING_COMPLETE: "SHUFFLING_COMPLETE"
};

const initGameState = {
    deck: [],
    gameEnded: false,
    gameStarted: false,
    playerHands: {},
    shuffling: true,
};

const cardGameStateReducer = (state, action) => {
    const { CARD_DEALT_TO_PLAYER, CARDS_RETURNED_TO_DECK, GAME_ENDED, GAME_STARTED, SHUFFLING_COMPLETE } = actions;
    switch (action.type) {
        case CARD_DEALT_TO_PLAYER: {
            const { userName, cardDealt } = action.payload;
            const handOfUser = state.playerHands[userName];
            return {
                ...state,
                deck: state.deck.filter((card) => card !== cardDealt),
                playerHands: { ...state.playerHands, [userName]: handOfUser.concat(cardDealt) }
            };
        }
        case CARDS_RETURNED_TO_DECK: {
            const { userName, cardsToReturn } = action.payload;
            return {
                ...state,
                deck: state.deck.concat(cardsToReturn),
                playerHands: {
                    ...state.playerHands,
                    [userName]: state.playerHands[userName].filter((card) => !cardsToReturn.includes(card))
                },
                shuffling: true,
            }
        }
        case GAME_ENDED: {
            return {
                ...state,
                gameEnded: true
            }
        }
        case GAME_STARTED: {
            const { deck, playerHands } = action.payload;
            return { ...state, deck, gameStarted: true, playerHands, shuffling: true };
        }
        case SHUFFLING_COMPLETE: {
            const deck = action.payload;
            return {
                ...state,
                deck,
                shuffling: false
            }
        }
        default: {
            throw Error(`oops, wrong action type ${action.type}`);
        }
    }
};


export const useCardGameState = (players, cards, maxCardsPerPlayer) => {
    const emptyPlayerHands = players.reduce((hands, player) => {
        hands[player.userName] = [];
        return hands;
    }, {});

    const [cardGameState, dispatch] = useReducer(cardGameStateReducer, { ...initGameState, deck: cards, playerHands: emptyPlayerHands });
    
    useEffect(() => {
        if (cardGameState.shuffling === false) {
            return;
        }
        const deck = cardGameState.deck;
        //shuffle cards randomly
        for (let x = 0; x < deck.length; x++) {
            const tmp = deck[x];
            const swapIndex = Math.floor(x + Math.random() * (deck.length - x));
            deck[x] = deck[swapIndex];
            deck[swapIndex] = tmp;
        }
        dispatch({
            type: actions.SHUFFLING_COMPLETE,
            payload: deck
        });
    }, [cardGameState.shuffling]);

    useEffect(() => {
        const deck = cardGameState.deck;
        setTimeout(() => {
            if (cardGameState.gameStarted === false) {
                const playerHands = players.reduce((hands, player) => {
                    hands[player.userName] = deck.splice(0, maxCardsPerPlayer);
                    return hands;
                }, {});
                dispatch({
                    type: actions.GAME_STARTED,
                    payload: { deck, playerHands }
                });
            }
        }, 1500);
    }, [cardGameState.gameStarted]);
    
    return {
        cardGameState,
        dealCardToPlayer: (userName, card) => {
            dispatch({
                type: actions.CARD_DEALT_TO_PLAYER,
                payload: { userName, cardDealt: card }
            });
        },
        returnCardsToDeck: (userName, cardsToReturn) => {
            dispatch({
                type: actions.CARDS_RETURNED_TO_DECK,
                payload: { userName, cardsToReturn }
            });
        }
    };
}